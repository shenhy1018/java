import java.util.Random;
public class UniformRandomNumberGenerator implements RandomVectorGenerator{

	private int N;
	
	public UniformRandomNumberGenerator(int N){
		this.N = N;
	}

	@Override
	public double[] getVector() {
		double[] vector = new double[N];
		Random random = new Random();
		for (int i = 0; i < vector.length; i++) {
			vector[i] = random.nextDouble();
		}
		return vector;
	}

}
