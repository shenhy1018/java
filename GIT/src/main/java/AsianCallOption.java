import java.util.List;

import org.joda.time.DateTime;

//the payout of asian option
public class AsianCallOption implements PayOut {
    private double K;
	
	public AsianCallOption(double K){
		this.K = K;
	}

	@Override
	public double getPayout(PathGenerator path) {
		List<Pair<DateTime, Double>>  prices = path.getPath();
		double average=0, sum=0;
		for (int i=0;i<prices.size();i++){
			sum+=prices.get(i).getValue();
		}
		average=sum/prices.size();
		return Math.max(0, average - K);
	}

}
