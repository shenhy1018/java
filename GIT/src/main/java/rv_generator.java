
public class rv_generator implements RandomVectorGenerator{
	//override the method in the interface
	private int size;
	//store the normal random number
	double [] normal_gen;
	float [] temp;
	//check how many numbers used in the 2000000 random numbers
	private int number=0;
    // generate 2000000 uniform random number
	private UniformRandomNumberGenerator uniform_gen;
	
	//constructor
	public rv_generator (int n){
        uniform_gen=new UniformRandomNumberGenerator(2000000);
		this.size=n;
		temp=GIT.GPU_processor(uniform_gen.getVector());
		normal_gen=new double[temp.length];

		for (int i=0;i<temp.length;i++){
			normal_gen[i]=(double)temp[i];
		}
	}
	
	public int get_size(){
		return size;
	}
	
	//this method is to store each array with normal numbers from the 2000000 numbers, and every time we generate a path
	//  we use 252 of them, if 2000000 numbers is used,
	//we generate a new set
	//override the method in the interface
	public double[] getVector(){
		double [] vector=new double[size];
		if (number<2000000-size){
			System.arraycopy(normal_gen,number,vector,0,size);
			number=number+size;
		}
		else {
			System.arraycopy(normal_gen, number ,vector, 0, 2000000 - number);
			int remaining=size-2000000+number;
			temp=GIT.GPU_processor(uniform_gen.getVector());
			for (int i=0;i<temp.length;i++){
				normal_gen[i]=(double)temp[i];
			}

			System.arraycopy(normal_gen, 0 ,vector,size-remaining , remaining);
			number =remaining;

		}


		return vector;
	}

}
