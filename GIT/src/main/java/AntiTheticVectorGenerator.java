public class AntiTheticVectorGenerator implements RandomVectorGenerator{
	
	private RandomVectorGenerator rvg;
	double[] lastVector=null;

	public AntiTheticVectorGenerator(RandomVectorGenerator rvg){
		this.rvg = rvg;
	}

	@Override
	public double[] getVector() {
		if ( lastVector == null ){
			lastVector = rvg.getVector();
			return lastVector;
		} else {
			double[] tmp=new double[lastVector.length];
			for (int i = 0; i < lastVector.length; i++){ tmp[i] = -1*lastVector[i];}
			lastVector = null;
			return tmp;
		}
	}
	
}

