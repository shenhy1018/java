import org.joda.time.DateTime;
//this class just price one european and asian option using gpu to generate normal random numbers
public class main1 {
	public static void main(String[] args){
		double rate=0.0001,sigma=0.01,S0=152.35,K=165.0,se=0.01,prob=0.96;
		int N=252;
		DateTime start=new DateTime(2014,10,18,0,0);
		DateTime end=start.plusDays(N);
		rv_generator rvg=new rv_generator(N);
		RandomVectorGenerator rvg1=new AntiTheticVectorGenerator(rvg);
		GBMRandomPathGenerator gen=new GBMRandomPathGenerator(rate, N,sigma,S0,start,end,rvg1);
		EuropeanCallOption option1=new EuropeanCallOption(K);
		simulation_manager manager1=new simulation_manager(rvg1,option1,gen,se,prob);
		double mean1=0;
		long begintime = System.currentTimeMillis();
		System.out.println("It takes about 6 mins for european option price to converge.");
		mean1=Math.exp(-1*rate*N)*manager1.run();
		long endtime=System.currentTimeMillis();
        long costTime = (endtime - begintime);
		System.out.println("The price of European option is about: "+mean1+"$.");
		System.out.println("It actually takes "+(int)costTime/60000+" mins to converge.");
		K=164.0;
		AsianCallOption option2=new AsianCallOption(K);
		simulation_manager manager2=new simulation_manager(rvg1,option2,gen,se,prob);
		double mean2=0;
		long begintime1 = System.currentTimeMillis();
		System.out.println("\r\n");
		System.out.println("It takes about 2 mins for asian option price to converge.");
		mean2=Math.exp(-1*rate*N)*manager2.run();
		long endtime1=System.currentTimeMillis();
        long costTime1= (endtime1 - begintime1);
        System.out.println("The price of European option is about: "+mean2+"$.");
		System.out.println("It actuallytakes "+(int)costTime1/60000+" mins to converge.");
		
		
	}
}
