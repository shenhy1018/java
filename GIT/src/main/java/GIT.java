
import com.nativelibs4java.opencl.*;
import org.bridj.Pointer;
import static org.bridj.Pointer.allocateDoubles;

import static org.bridj.Pointer.allocateFloats;

/**
 * Created with IntelliJ IDEA.
 * User: eran
 * Date: 8/24/13
 * Time: 2:30 PM
 * To change this template use File | Settings | File Templates.
 */
public class GIT {
   //using GPU to convert uniform numbers to normal rvs
    public static float[] GPU_processor(double [] uniform){
        int size=uniform.length;
        float []normal_num=new float [size];
        // Creating the platform which is out computer.
        CLPlatform clPlatform = JavaCL.listPlatforms()[0];
        // Getting the GPU device
        CLDevice device = clPlatform.getBestDevice();
        //CLDevice device = clPlatform.listGPUDevices(true)[0];
        // Verifing that we have the GPU device
        CLContext context = JavaCL.createContext(null, device);
        CLQueue queue = context.createDefaultQueue();

        // Read the program sources and compile them :
        //using boxer muller method to generate normal random numbers
        String src = "__kernel void uniform_to_normal(__global float* a, __global float* b, int n) \n" +
                "{\n" +
                "    int i = get_global_id(0);\n" +
                "    if (i>=n/2)\n" +
                "        return;\n" +
                "\n" +
                "b[2*i]=sqrt(-2*log(a[2*i]))*cos(2*3.1415926*a[2*i+1]);\n" +
                "b[2*i+1]=sqrt(-2*log(a[2*i]))*sin(2*3.1415926*a[2*i+1]);\n" +
                "}";
        CLProgram program = context.createProgram(src);
        program.build();

        CLKernel kernel = program.createKernel("uniform_to_normal");


        final Pointer<Float>
                aPtr = allocateFloats(size);



        for (int i = 0; i < size; i++) {
            aPtr.set(i,(float)uniform[i] );
        }


        // Create OpenCL input buffers (using the native memory pointers aPtr and bPtr) :
        CLBuffer<Float>
                a = context.createFloatBuffer(CLMem.Usage.Input, aPtr);

        // Create an OpenCL output buffer :
        CLBuffer<Float> out = context.createFloatBuffer(CLMem.Usage.Output, size);
        kernel.setArgs(a,out,size);
        CLEvent event = kernel.enqueueNDRange(queue, new int[]{size}, new int[]{128});
        //read the normal random numbers from the event
        Pointer<Float> aPtr1 = out.read(queue, event);
        //format the required normal number array
        normal_num = aPtr1.getFloats();
        //release the memory
        a.release();
        out.release();
        aPtr1.release();
        aPtr.release();



        return normal_num;











    }

}
