public class Stats_Collector {
	private double mean=0;
	private double mean_sq=0;
	private int num=0;
	private double var=0;
	
	public Stats_Collector(){
		mean=0;
		mean_sq=0;
		num=0;
	}
	
	
	public double get_mean(){return mean;}
	public double get_variance(){return var;}
	public int get_num(){return num;}
	public void clear(){
		mean=0;
		num=0;
		mean_sq=0;
		var=0;
	}
	public void calculate(double x){
		this.num++;
		mean=mean*(double)(num-1)/(double)num+x*1/(double)num;
		mean_sq=mean_sq*(double)(num-1)/(double)num+x*x*1/(double)num;
		var=mean_sq-mean*mean;
	}
	

    
}
