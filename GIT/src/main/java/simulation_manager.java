//import org.apache.commons.math3.*;
import org.apache.commons.math3.distribution.NormalDistribution;

public class simulation_manager {
	private RandomVectorGenerator rvg;
	private PayOut payout;
	private PathGenerator gen;
	double standard_dev;
	double prob;
	
	
	public simulation_manager(RandomVectorGenerator rvg1,PayOut payout1,PathGenerator gen1,double a,double b){
		this.rvg=rvg1;
		this.payout=payout1;
		this.gen=gen1;
		this.standard_dev=a;
		this.prob=b;
	}
	
	
	public double run(){
		Stats_Collector sc=new Stats_Collector();
		NormalDistribution nor=new NormalDistribution();
		double critical_value=nor.inverseCumulativeProbability(1-(1-prob)/2);
		int iteration=1;
		for(;iteration<10000;iteration++){
			sc.calculate(payout.getPayout(gen));
		}

		while(Math.sqrt(sc.get_variance())*critical_value/(Math.sqrt(iteration))>standard_dev){
			sc.calculate(payout.getPayout(gen));
			iteration++;
		}
		System.out.println("The convergence requires: "+iteration+" iterations.");
		return sc.get_mean();
	}
	
	

}
