import java.util.List;

import org.joda.time.DateTime;

import junit.framework.TestCase;
//this class printout one GBM path to test the correctness of the GBM generator
public class Test_GBM extends TestCase {
	public void test_gbm(){
		//test GBMRandomPathGenerator with the following example
		double rate=0.0001;
		int N=252;
		double sigma=0.01;
		double S0=152.35;
		DateTime startDate=new DateTime(2014,10,18,0,0);
		DateTime endDate=startDate.plusDays(N);
		rv_generator rvg=new rv_generator(N);
		GBMRandomPathGenerator gen=new GBMRandomPathGenerator(rate,N,sigma,S0,startDate,endDate,rvg);
		List<Pair<DateTime,Double>>prices=gen.getPath();
		//print out the example path
		System.out.println("The test path is: ");
		for (int i=0;i<prices.size();i++){
			System.out.println(prices.get(i).getValue()+"$");
		}
	}

}
