import junit.framework.TestCase;

public class Test_statscollector extends TestCase {
	public void test_sc(){
		Stats_Collector sc= new Stats_Collector();
		sc.calculate(1.0);
		assertTrue(sc.get_mean()-1<0.001&&sc.get_variance()-1<0.001&&sc.get_num()-1<0.001);
		sc.calculate(0.0);
		assertTrue(sc.get_mean()-0.5<0.001&&sc.get_variance()-0.75<0.001&&sc.get_num()-2<0.001);
		sc.clear();
		assertTrue(sc.get_mean()==0&&sc.get_variance()==0&&sc.get_num()==0);
		System.out.println("statscollector works well.");
	}


	

}
