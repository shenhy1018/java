import org.joda.time.DateTime;

import junit.framework.TestCase;
//test whether europeancalloption, asiancalloption and simulationmanager work well.
public class Test_simulationmanager extends TestCase {
	public void test_simulationmanager(){
		double rate=0.0001,sigma=0.01,S0=152.35,K=165.0,se=0.01,prob=0.96;
		int N=252;
		DateTime start=new DateTime(2014,10,18,0,0);
		DateTime end=start.plusDays(N);
		rv_generator rvg=new rv_generator(N);
		RandomVectorGenerator rvg1=new AntiTheticVectorGenerator(rvg);
		GBMRandomPathGenerator gen=new GBMRandomPathGenerator(rate, N,sigma,S0,start,end,rvg1);
		EuropeanCallOption option1=new EuropeanCallOption(K);
		simulation_manager manager1=new simulation_manager(rvg1,option1,gen,se,prob);
		double mean1=0;
		mean1=Math.exp(-1*rate*N)*manager1.run();
		System.out.println("The price of European option is about: "+mean1+"$.");
		
		K=164.0;
		AsianCallOption option2=new AsianCallOption(K);
		simulation_manager manager2=new simulation_manager(rvg1,option2,gen,se,prob);
		double mean2=0;
        System.out.println("\r\n");
		mean2=Math.exp(-1*rate*N)*manager2.run();
		System.out.println("The price of European option is about: "+mean2+"$.");

	}

}
