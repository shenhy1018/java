import junit.framework.TestCase;
//print out 10 normal random numbers to test the correctness of the rv_generator
public class Test_rv_generator extends TestCase {
	public void test_rv_generator(){
		rv_generator vector1=new rv_generator(10);
		assertTrue(vector1.get_size()==10);
		double [] gaussian_vector=vector1.getVector();
		System.out.println("The vector with 10 standard normal simulations is: ");
		for (int i=0;i<gaussian_vector.length;i++){
			System.out.println(gaussian_vector[i]);
		
	    }
	}
  
}
