import junit.framework.TestCase;

public class Test_Antithetic extends TestCase {
	public void test_antithetic(){
		UniformRandomNumberGenerator rvg=new UniformRandomNumberGenerator(10);
        //test constructor of uniformrandomnumbergenerator
        double []rvg2=rvg.getVector();
		assertTrue(rvg2.length==10);

		//test constructor of antithetic 
		AntiTheticVectorGenerator rvg1=new AntiTheticVectorGenerator(rvg);
	    double []rvg3=rvg1.getVector();
	    double []rvg4=rvg1.getVector();
		assertTrue(rvg3.length==10);
		assertTrue(rvg4.length==10);
        boolean test=true;

        //test the getVector method in the antithetic
        for (int i=0;i<rvg2.length;i++){
        	if (rvg3[i]-(-1*rvg4[i])>0.001){
        		test=false;
        	}
        }
        if (test==true){System.out.println("Antithetic decorator works well");}
        
        
		
	}

}
